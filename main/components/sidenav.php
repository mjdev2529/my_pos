<?php
  if($page == page_url('dashboard')){
    $dashboard = "active";
    $sales = "";
    $products = "";
    $suppliers = "";
    $stocks = "";
    $customers = "";
    $sales_report = "";
    $inv_report = "";
    $p_return = "";
    $s_return = "";
    $sum_report = "";
    $users = "";
    $branches = "";
    $cash_adjustment = "";
    $sales_listing = "";
  }else if($page == page_url('sales')){
    $dashboard = "";
    $sales = "active";
    $products = "";
    $suppliers = "";
    $stocks = "";
    $customers = "";
    $sales_report = "";
    $inv_report = "";
    $p_return = "";
    $s_return = "";
    $sum_report = "";
    $users = "";
    $branches = "";
    $cash_adjustment = "";
    $sales_listing = "";
  }else if($page == page_url('products')){
    $dashboard = "";
    $sales = "";
    $products = "active";
    $suppliers = "";
    $stocks = "";
    $customers = "";
    $sales_report = "";
    $inv_report = "";
    $p_return = "";
    $s_return = "";
    $sum_report = "";
    $users = "";
    $branches = "";
    $cash_adjustment = "";
    $sales_listing = "";
  }else if($page == page_url('suppliers')){
    $dashboard = "";
    $sales = "";
    $products = "";
    $suppliers = "active";
    $stocks = "";
    $customers = "";
    $sales_report = "";
    $inv_report = "";
    $p_return = "";
    $s_return = "";
    $sum_report = "";
    $users = "";
    $branches = "";
    $cash_adjustment = "";
    $sales_listing = "";
  }else if($page == page_url('stocks')){
    $dashboard = "";
    $sales = "";
    $products = "";
    $suppliers = "";
    $stocks = "active";
    $customers = "";
    $sales_report = "";
    $inv_report = "";
    $p_return = "";
    $s_return = "";
    $sum_report = "";
    $users = "";
    $branches = "";
    $cash_adjustment = "";
    $sales_listing = "";
  }else if($page == page_url('customers')){
    $dashboard = "";
    $sales = "";
    $products = "";
    $suppliers = "";
    $stocks = "";
    $customers = "active";
    $sales_report = "";
    $inv_report = "";
    $p_return = "";
    $s_return = "";
    $sum_report = "";
    $users = "";
    $branches = "";
    $cash_adjustment = "";
    $sales_listing = "";
  }else if($page == page_url('sales_report')){
    $dashboard = "";
    $sales = "";
    $products = "";
    $suppliers = "";
    $stocks = "";
    $customers = "";
    $sales_report = "active";
    $inv_report = "";
    $p_return = "";
    $s_return = "";
    $sum_report = "";
    $users = "";
    $branches = "";
    $cash_adjustment = "";
    $sales_listing = "";
  }else if($page == page_url('inventory_report')){
    $dashboard = "";
    $sales = "";
    $products = "";
    $suppliers = "";
    $stocks = "";
    $customers = "";
    $sales_report = "";
    $inv_report = "active";
    $p_return = "";
    $s_return = "";
    $sum_report = "";
    $users = "";
    $branches = "";
    $cash_adjustment = "";
    $sales_listing = "";
  }else if($page == page_url('p_return')){
    $dashboard = "";
    $sales = "";
    $products = "";
    $suppliers = "";
    $stocks = "";
    $customers = "";
    $sales_report = "";
    $inv_report = "";
    $p_return = "active";
    $s_return = "";
    $sum_report = "";
    $users = "";
    $branches = "";
    $cash_adjustment = "";
    $sales_listing = "";
  }else if($page == page_url('s_return')){
    $dashboard = "";
    $sales = "";
    $products = "";
    $suppliers = "";
    $stocks = "";
    $customers = "";
    $sales_report = "";
    $inv_report = "";
    $p_return = "";
    $s_return = "active";
    $sum_report = "";
    $users = "";
    $branches = "";
    $cash_adjustment = "";
    $sales_listing = "";
  }else if($page == page_url('summary_report')){
    $dashboard = "";
    $sales = "";
    $products = "";
    $suppliers = "";
    $stocks = "";
    $customers = "";
    $sales_report = "";
    $inv_report = "";
    $p_return = "";
    $s_return = "";
    $sum_report = "active";
    $users = "";
    $branches = "";
    $cash_adjustment = "";
    $sales_listing = "";
  }else if($page == page_url('users')){
    $dashboard = "";
    $sales = "";
    $products = "";
    $suppliers = "";
    $stocks = "";
    $customers = "";
    $sales_report = "";
    $inv_report = "";
    $p_return = "";
    $s_return = "";
    $sum_report = "";
    $users = "active";
    $branches = "";
    $cash_adjustment = "";
    $sales_listing = "";
  }else if($page == page_url('branches')){
    $dashboard = "";
    $sales = "";
    $products = "";
    $suppliers = "";
    $stocks = "";
    $customers = "";
    $sales_report = "";
    $inv_report = "";
    $p_return = "";
    $s_return = "";
    $sum_report = "";
    $users = "";
    $branches = "active";
    $cash_adjustment = "";
    $sales_listing = "";
  }else if($page == page_url('cash_adjustment')){
    $dashboard = "";
    $sales = "";
    $products = "";
    $suppliers = "";
    $stocks = "";
    $customers = "";
    $sales_report = "";
    $inv_report = "";
    $p_return = "";
    $s_return = "";
    $sum_report = "";
    $users = "";
    $branches = "";
    $cash_adjustment = "active";
    $sales_listing = "";
  }else if($page == page_url('sales_listing')){
    $dashboard = "";
    $sales = "";
    $products = "";
    $suppliers = "";
    $stocks = "";
    $customers = "";
    $sales_report = "";
    $inv_report = "";
    $p_return = "";
    $s_return = "";
    $sum_report = "";
    $users = "";
    $branches = "";
    $cash_adjustment = "";
    $sales_listing = "active";
  }
?>
 <div class="sidebar-sticky pt-3">
    <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
      <span>master data</span>
    </h6>
    <ul class="nav flex-column mb-2">
      <?php if($_SESSION["role"] == 0){?>

        <li class="nav-item">
          <a class="nav-link h6 <?=$users?>" href="index.php?page=<?=page_url('users')?>">
            <span class="fa fa-user"></span>
            Users
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link h6 <?=$branches?>" href="index.php?page=<?=page_url('branches')?>">
            <span class="fa fa-warehouse"></span>
            Branches
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link h6 <?=$customers?>" href="index.php?page=<?=page_url('customers')?>">
            <span class="fa fa-users"></span>
            Customers
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link h6 <?=$products?>" href="index.php?page=<?=page_url('products')?>">
            <span class="fa fa-cubes"></span>
            Products
          </a>
        </li>
        <!-- <li class="nav-item">
          <a class="nav-link h6 <?=$suppliers?>" href="index.php?page=<?=page_url('suppliers')?>">
            <span class="fa fa-truck-moving"></span>
            Suppliers
          </a>
        </li> -->
      <?php } ?>
    </ul>

  	<h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
      <span>transactions</span>
    </h6>
    <ul class="nav flex-column mb-2">
      <!-- <li class="nav-item">
        <a class="nav-link h6 <?=$dashboard?>" href="index.php?page=<?=page_url('dashboard')?>">
          <span class="fa fa-home"></span>
          Dashboard <span class="sr-only">(current)</span>
        </a>
      </li> -->
      <li class="nav-item">
        <a class="nav-link h6 <?=$sales?>" href="index.php?page=<?=page_url('sales')?>">
          <span class="fa fa-shopping-cart"></span>
          Sales
        </a>
      </li>
      <?php if($_SESSION["role"] == 0){?>

        <li class="nav-item">
          <a class="nav-link h6 <?=$p_return?>" href="index.php?page=<?=page_url('p_return')?>">
            <span class="fa fa-ban"></span>
            Cancel Sales Order
          </a>
        </li>
        
        <li class="nav-item">
        <a class="nav-link h6 <?=$cash_adjustment?>" href="index.php?page=<?=page_url('cash_adjustment')?>">
          <span class="fa fa-money-bill"></span>
          Cash Adjustment
        </a>
      </li>
        <li class="nav-item">
          <a class="nav-link h6 <?=$stocks?>" href="index.php?page=<?=page_url('stocks')?>">
              <span class="fa fa-dolly"></span>
              Receiving Stocks
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link h6 <?=$s_return?>" href="index.php?page=<?=page_url('s_return')?>">
            <span class="fa fa-boxes"></span>
            Stocks Adjustment
          </a>
        </li>
      <?php } ?>
    </ul>

    <?php if($_SESSION["role"] == 0){?>
      <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
        <span>reports</span>
      </h6>
      <ul class="nav flex-column mb-2">
        <li class="nav-item">
         <a class="nav-link h6 <?=$sales_listing?>" href="index.php?page=<?=page_url('sales_listing')?>">
            <span class="fa fa-list"></span>
            Sales Listing
          </a>
        </li>
        <li class="nav-item">
         <a class="nav-link h6 <?=$sales_report?>" href="index.php?page=<?=page_url('sales_report')?>">
            <span class="fa fa-chart-bar"></span>
            Sales Report
          </a>
        </li>
        <li class="nav-item">
         <a class="nav-link h6 <?=$inv_report?>" href="index.php?page=<?=page_url('inventory_report')?>">
            <span class="fa fa-archive"></span>
            Inventory Report
          </a>
        </li>
        <li class="nav-item">
         <a class="nav-link h6 <?=$sum_report?>" href="index.php?page=<?=page_url('summary_report')?>">
            <span class="far fa-file-alt"></span>
            Summary Report
          </a>
        </li>
      </ul>
      <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
        <span>back up</span>
      </h6>
      <ul class="nav flex-column mb-2">
        <li class="nav-item">
         <a class="nav-link h6" href="#" onclick="back_up()">
            <span class="fa fa-file-download"></span>
            Back up
          </a>
        </li>
      </ul>
    <?php } ?>
</div>