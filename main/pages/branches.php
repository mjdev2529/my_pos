<div class="main">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2"> <span class="text-dark">Branches</span></h1>
    <div class="btn-toolbar mb-2 mb-md-0">
      <div class="h5 mr-5">
        <i class="fa fa-user mr-1"></i> Welcome: <?=$_SESSION["name"];?>
      </div>
      <div class="h5">
        <i class="far fa-calendar mr-1"></i> <?=date("F d, Y");?>
      </div>
    </div>
  </div>

  <div class="row mb-2 card">
     <div class="col-12 bg-light p-2">
      <div class="btn-group mb-3 float-right">
        <button class="btn btn-sm btn-outline-success" data-toggle="modal" data-target="#add_branch">Add</button>
        <button class="btn btn-sm btn-outline-danger" onclick="delete_branch()">Delete</button>
      </div>
      <div class="table-responsive">
        <table id="tbl_products" class="table table-striped table-bordered table-sm">
          <thead>
            <tr>
              <th width="15"><input type="checkbox" id="checkBranch" onclick="checkAll()"></th>
              <th width="15">#</th>
              <th>Name</th>
              <th>Address</th>
              <th>Contact No.</th>
              <th width="100">Action</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="add_branch" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-plus"></i> Add new Branch</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="add_branch_form">
          <div class="row">
            <div  class="col-8 offset-2 mb-3">
              <label>Name</label>
              <input type="text" name="name" class="form-control" placeholder="Name" required="">
            </div>
            <div  class="col-8 offset-2 mb-3">
              <label>Address</label>
              <textarea class="form-control" name="address" placeholder="type here..."></textarea>
            </div>
            <div  class="col-8 offset-2 mb-3">
              <label>Contact No.</label>
              <input type="text" name="contact_no" class="form-control" placeholder="Contact Number" maxlength="11">
            </div>
            <div class="col-12 p-0">
              <hr>
              <div class="float-right pr-2">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>  
              </div>
            </div>
          </div>      
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="edit_branch" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-edit"></i> Edit Branch</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="edit_branch_form">
          <div class="row">
            <div  class="col-8 offset-2 mb-3">
              <label>Name</label>
              <input type="text" name="branch_name" id="branch_name" class="form-control" placeholder="Name" required="">
              <input type="hidden" name="branch_id" id="branch_id">
            </div>
            <div  class="col-8 offset-2 mb-3">
              <label>Address</label>
              <textarea class="form-control" name="branch_address" id="branch_address" placeholder="type here..."></textarea>
            </div>
            <div  class="col-8 offset-2 mb-3">
              <label>Contact No.</label>
              <input type="text" name="branch_contact" id="branch_contact" class="form-control" placeholder="Contact Number" maxlength="11">
            </div>
            <div class="col-12 p-0">
              <hr>
              <div class="float-right pr-2">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>  
              </div>
            </div>
          </div>      
        </form>
      </div>
    </div>
  </div>
</div>

<!-- PAGE SCRIPT -->
<script type="text/javascript">
  $(document).ready( function(){
    get_branches();
  });

  function checkAll(){
    var x = $("#checkBranch").is(":checked");

    if(x){
      $("input[name=cb_branch]").prop("checked", true);
    }else{
      $("input[name=cb_branch]").prop("checked", false);
    }
  }

  function get_branches(){
    $("#tbl_products").DataTable().destroy();
    $("#tbl_products").dataTable({
      "ajax": {
        "type": "POST",
        "url": "../ajax/datatables/branch_data.php",
      },
      "processing": true,
      "columns": [
      {
        "mRender": function(data, type, row){
          return "<input type='checkbox' value='"+row.branch_id+"' name='cb_branch'>";
        }
      },
      {
        "data": "count"
      },
      {
        "data": "branch_name"
      },
      {
        "data": "branch_address"
      },
      {
        "data": "branch_contact"
      },
      {
        "mRender": function(data, type, row){
          return "<button class='btn btn-sm btn-outline-dark' onclick='edit_branch("+row.branch_id+")'>Edit Branch</button>";
        }
      }
      ]

    });
  }

  $("#add_branch_form").submit( function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var url = "../ajax/branch_add.php";
    $.ajax({
      type: "POST",
      url: url,
      data: data,
      success: function(data){
        if(data == 1){
          alert("Success! New branch was added.");
          $("#add_branch").modal("hide");
          $("input").val("");
          $("textarea").val("");
          get_branches();
        }else{
          alert("Error: Something wrong.");
        }
      }
    });
  });

  function edit_branch(branch_id){
    var url = "../ajax/branch_details.php";
    $.ajax({
      type: "POST",
      url: url,
      data: {branch_id: branch_id},
      success: function(data){
        $("#edit_branch").modal();
        var o = JSON.parse(data);
        $("#branch_name").val(o.branch_name);
        $("#branch_address").val(o.branch_address);
        $("#branch_contact").val(o.branch_contact);
        $("#branch_id").val(branch_id);
      }
    });
  }

  $("#edit_branch_form").submit( function(e){
    e.preventDefault();

    var data = $(this).serialize();
    var url = "../ajax/branch_edit.php";
    $.ajax({
      type: "POST",
      url: url,
      data: data,
      success: function(data){
        if(data == 1){
          alert("Success! branch was updated.");
          $("#edit_branch").modal("hide");
          $("input").val("");
          $("textarea").val("");
          get_branches();
        }else{
          alert("Error: Something wrong.");
        }
      }
    });
  });

  function delete_branch(){
    var conf = confirm("Are you sure to delete selected?");
    if(conf){
      var br_id = [];

      $("input[name=cb_branch]:checked").each( function(){
        br_id.push($(this).val());
      });

      if(br_id.length != 0){

        var url = "../ajax/branch_delete.php";

        $.ajax({
          type: "POST",
          url: url,
          data: {br_id: br_id},
          success: function(data){
            if(data != 0){
              alert("Success! Selected branch/es was deleted.");
              get_branches();
            }else{
              alert("Error: Something wrong.");
            }
          }
        });
      }else{
        alert("Warning! No data selected.");
      }
    }
  }

</script>
