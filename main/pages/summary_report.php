<div class="main">

  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2"> <span class="text-dark">Summary Report</span></h1>
    <div class="btn-toolbar mb-2 mb-md-0">
      <div class="h5 mr-5">
        <i class="fa fa-user mr-1"></i> Welcome: <?=$_SESSION["name"];?>
      </div>
      <div class="h5">
        <i class="far fa-calendar mr-1"></i> <?=date("F d, Y");?>
      </div>
    </div>
  </div>

  <div class="row mb-2 card">
    <div class="col-12 mb-3 bg-light p-2">
      <form class="row" id="form_generate">
        <div class="col-1 offset-2 text-right h5 p-0 pt-2">From: </div>
        <div class="col-2"><input type="date" class="form-control" name="from_date" value="<?=date('Y-m-d')?>"></div>
        <div class="col-1 text-right h5 p-0 pt-2">Summary: </div>
        <div class="col-2">
          <select id="s_type" class="form-control">
            <option value="0">Select Summary:</option>
            <option value="1">Cash Summary</option>
            <option value="2">Credit Type Summary</option>
          </select>
        </div>
        <div class="col-2"><button type="submit" class="btn btn-primary"><i class="fa fa-sync-alt"></i> Generate</button></div>
      </form>
      <hr>
    </div>

    <div class="col-12 report-container p-2">
      <!-- <h3 class="col-6 offset-3">Sales Report from <span id="from-date"><?=date('Y-m-d')?></span> to <span id="to-date"><?=date('Y-m-d')?></span></h3> -->
      <div class="table-responsive">
      </div>
    </div>

  </div>

</div>

<!-- PAGE SCRIPT -->
<script type="text/javascript">
  $(document).ready( function(){
    sales_report("<?=date('Y-m-d')?>",0);
  });

  function sales_report(fromDate,sType){
    if(sType == 1){
      var url = "../ajax/datatables/cash_summary_data.php";
    }else{
      var url = "../ajax/datatables/credit_summary_data.php";
    }

    if(sType != 0){
      $.ajax({
        type: "POST",
        url: url,
        data: {fromDate: fromDate},
        success: function(data){
          $(".table-responsive").html(data);
        }
      });
    }else{
      $(".table-responsive").html("<div class='col-12 text-center h3'><i class='fa fa-info-circle'></i> No Data Available.</div>");
    }

  }

  $("#form_generate").submit( function(e){
    e.preventDefault();
    var from = $("input[name=from_date]").val();
    var s_type = $("#s_type").val();
    sales_report(from,s_type);
  });

    function printDiv() 
  {

    var divToPrint=document.getElementById('summary_container');

    var newWin=window.open('','Print-Window');

    newWin.document.open();

    newWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css"></head><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

    newWin.document.close();

    setTimeout(function(){newWin.close();},10);

  }

</script>