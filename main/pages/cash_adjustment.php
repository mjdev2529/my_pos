<div class="main">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2"> <span class="text-dark">Cash Adjustment</span></h1>
    <div class="btn-toolbar mb-2 mb-md-0">
      <div class="h5 mr-5">
        <i class="fa fa-user mr-1"></i> Welcome: <?=$_SESSION["name"];?>
      </div>
      <div class="h5">
        <i class="far fa-calendar mr-1"></i> <?=date("F d, Y");?>
      </div>
    </div>
  </div>

  <div class="row mb-2 card">
     <div class="col-12 bg-light p-2">
      <div class="btn-group mb-3 float-right">
        <button class="btn btn-sm btn-outline-success" data-toggle="modal" data-target="#add_cash_adjustment">Add</button>
        <button class="btn btn-sm btn-outline-danger" onclick="delete_cash_adjustment()">Delete</button>
      </div>
      <div class="table-responsive">
        <table id="tbl_cash_adjustment" class="table table-striped table-bordered table-sm">
          <thead>
            <tr>
              <th width="15"><input type="checkbox" id="checkAdjustment" onclick="checkAll()"></th>
              <th width="15">#</th>
              <th>Adjustment Type</th>
              <th>Remarks</th>
              <th width="100">Amount</th>
              <th width="150">Added By</th>
              <th width="150">Date Added</th>
              <!-- <th width="100">Action</th> -->
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="add_cash_adjustment" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-plus"></i> Add new Adjustment</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="add_adjustment_form">
          <div class="row">
            <div  class="col-8 offset-2 mb-3">
              <label>Type</label>
              <select name="a_type" class="form-control" required="">
                <option value="0">Select Adjustment:</option>
                <option value="1">Cash Disbursement</option>
                <option value="2">Cash In</option>
                <option value="3">Cash Out</option>
                <option value="4">Cash Payment</option>
              </select>
            </div>
            <div  class="col-8 offset-2 mb-3">
              <label>Remarks</label>
              <textarea class="form-control" name="remarks" placeholder="type here..." required=""></textarea>
            </div>
            <div  class="col-8 offset-2 mb-3">
              <label>Amount</label>
              <input type="number" name="amount" class="form-control" placeholder="Amount" required="">
            </div>
            <div class="col-12 p-0">
              <hr>
              <div class="float-right pr-2">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>  
              </div>
            </div>
          </div>      
        </form>
      </div>
    </div>
  </div>
</div>

<!-- <div class="modal fade" id="edit_product" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-edit"></i> Edit Product</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="edit_product_form">
          <div class="row">
            <div  class="col-8 offset-2 mb-3">
              <label>Brand Name</label>
              <input type="text" name="brand_name" id="brand_name" class="form-control" placeholder="Brand Name">
              <input type="hidden" name="product_id" id="product_id">
            </div>
            <div  class="col-8 offset-2 mb-3">
              <label>Category / Description</label>
              <textarea class="form-control" name="category_description" id="category_description" placeholder="type here..."></textarea>
            </div>
            <div  class="col-8 offset-2 mb-3">
              <label>Selling Price</label>
              <input type="number" name="product_price" id="product_price" class="form-control" placeholder="Product Price" step=".01">
            </div>
            <div class="col-12 p-0">
              <hr>
              <div class="float-right pr-2">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>  
              </div>
            </div>
          </div>      
        </form>
      </div>
    </div>
  </div>
</div> -->

<!-- PAGE SCRIPT -->
<script type="text/javascript">
  $(document).ready( function(){
    get_adjustments();
  });

  function checkAll(){
    var x = $("#checkAdjustment").is(":checked");

    if(x){
      $("input[name=cb_adjustment]").prop("checked", true);
    }else{
      $("input[name=cb_adjustment]").prop("checked", false);
    }
  }

  function get_adjustments(){
    $("#tbl_cash_adjustment").DataTable().destroy();
    $("#tbl_cash_adjustment").dataTable({
      "ajax": {
        "type": "POST",
        "url": "../ajax/datatables/cash_adjustment_data.php",
      },
      "processing": true,
      "columns": [
      {
        "mRender": function(data, type, row){
          return "<input type='checkbox' value='"+row.cash_adjustment_id+"' name='cb_adjustment'>";
        }
      },
      {
        "data": "count"
      },
      {
        "data": "type"
      },
      {
        "data": "remarks"
      },
      {
        "data": "amount"
      },
      {
        "data": "added_by"
      },
      {
        "data": "date_added"
      }
      // ,{
      //   "mRender": function(data, type, row){
      //     return "<button class='btn btn-sm btn-outline-dark' onclick='edit_product("+row.product_id+")'>Edit Product</button>";
      //   }
      // }
      ]

    });
  }

  $("#add_adjustment_form").submit( function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var url = "../ajax/cash_adjustment_add.php";
    $.ajax({
      type: "POST",
      url: url,
      data: data,
      success: function(data){
        if(data == 1){
          alert("Success! New cash adjustment was added.");
          $("#add_cash_adjustment").modal("hide");
          $("input").val("");
          $("textarea").val("");
          $("select").val("0");

          get_adjustments();
        }else{
          alert("Error: "+data);
        }
      }
    });
  });

  // function edit_product(product_id){
  //   var url = "../ajax/product_details.php";
  //   $.ajax({
  //     type: "POST",
  //     url: url,
  //     data: {product_id: product_id},
  //     success: function(data){
  //       $("#edit_product").modal();
  //       var o = JSON.parse(data);
  //       $("#brand_name").val(o.brand_name);
  //       $("#generic_name").val(o.generic_name);
  //       $("#category_description").val(o.category_description);
  //       var product_price = o.price;
  //       var gross_price = o.gross_price;
  //       $("#gross_price").val(gross_price);
  //       $("#product_price").val(product_price);
  //       var is_vatable = o.is_vatable==1?true:false;
  //       $("#is_vatable").prop("checked",is_vatable);
  //       var is_discountable = o.is_discountable==1?true:false;
  //       $("#is_discountable").prop("checked",is_discountable);
  //       $("#product_id").val(product_id);
  //     }
  //   });
  // }

  // $("#edit_product_form").submit( function(e){
  //   e.preventDefault();
  //   var x = $("#is_vatable").is(":checked");
  //   var is_vatable = x?1:0;

  //   var x = $("#is_discountable").is(":checked");
  //   var is_discountable = x?1:0;

  //   var data = $(this).serialize()+"&is_vatable="+is_vatable+"&is_discountable="+is_discountable;
  //   var url = "../ajax/product_edit.php";
  //   $.ajax({
  //     type: "POST",
  //     url: url,
  //     data: data,
  //     success: function(data){
  //       if(data == 1){
  //         alert("Success! Product was updated.");
  //         $("#edit_product").modal("hide");
  //         $("input").val("");
  //         $("textarea").val("");
  //         get_adjustments();
  //       }else{
  //         alert("Error: "+data);
  //       }
  //     }
  //   });
  // });

  function delete_cash_adjustment(){
    var conf = confirm("Are you sure to delete selected?");
    if(conf){
      var adjustment_id = [];

      $("input[name=cb_adjustment]:checked").each( function(){
        adjustment_id.push($(this).val());
      });

      if(adjustment_id.length != 0){

        var url = "../ajax/cash_adjustment_delete.php";

        $.ajax({
          type: "POST",
          url: url,
          data: {adjustment_id: adjustment_id},
          success: function(data){
            if(data != 0){
              alert("Success! Selected cash adjustment/s was deleted.");
              get_adjustments();
            }else{
              alert("Error: "+data);
            }
          }
        });
      }else{
        alert("Warning! No data selected.");
      }
    }
  }

</script>
