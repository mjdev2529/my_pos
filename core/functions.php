<?php

	function page_url($page){
		return md5(base64_encode($page));
	}

	function enCrypt($data){
		return base64_encode($data);
	}

	function deCrypt($data){
		return base64_decode($data);
	}

	function get_customer_name($customer_id, $conn){
		$customer_data = mysqli_fetch_array(mysqli_query($conn, "SELECT customer_name FROM tbl_customers WHERE customer_id = '$customer_id'"));
		return isset($customer_data[0])?$customer_data[0]:"";
	}

	function get_product_name($product_id, $conn){
		$product_data = mysqli_fetch_array(mysqli_query($conn, "SELECT brand_name FROM tbl_products WHERE product_id = '$product_id'"));
		return $product_data[0];
	}

	function get_product_price($product_id, $conn){
		$product_data = mysqli_fetch_array(mysqli_query($conn, "SELECT price FROM tbl_products WHERE product_id = '$product_id'"));
		return $product_data[0];
	}

	function get_detail_amount($sales_id, $conn){
		$sales_data = mysqli_query($conn, "SELECT * FROM tbl_sales_order a 
		INNER JOIN tbl_sales_order_detail b on a.sales_order_id = b.sales_order_id 
		INNER JOIN tbl_products c on c.product_id = b.product_id 
		where a.sales_order_id = '$sales_id' AND a.status = 1");
		$total_amount = 0;
		$total_vat=0;
		$total_vatable_price=0;
		$total_non_vatable_price=0;
		$total_discount=0;
		while($row = mysqli_fetch_array($sales_data)){
		
			$discount = 0;
	 
	        $price = $row["selling_price"];
			$vat = 0;
			$non_vat = $row["selling_price"];
		
	        $total_price = ($row["quantity"]-$row["returned_quantity"]) * $price;
	        $total_vatable_price += ($row["quantity"]-$row["returned_quantity"]) * $vat;
			$total_non_vatable_price +=$row["quantity"]* $non_vat;
	        $total_discount += ($row["quantity"]-$row["returned_quantity"]) * $discount;
		}
	
		$total_vat = $total_vatable_price *0.12;
		$total_amount=  $total_vatable_price+$total_vat+$total_non_vatable_price-$total_discount;

		return $total_amount; 
	}

	function get_balance_qty($product_id, $date, $conn){
		$get_stock_qty = mysqli_fetch_array(mysqli_query($conn, "SELECT sum(quantity) as qty FROM `tbl_stocks` WHERE date_added <= '$date' AND product_id = '$product_id'"));
		$get_adj_qty = mysqli_fetch_array(mysqli_query($conn, "SELECT sum(qty) as aqty FROM `tbl_stocks_adjustment` WHERE date_added <= '$date' AND product_id = '$product_id'"));
		$s_qty = $get_stock_qty[0]?$get_stock_qty[0]:0;
		$a_qty = $get_adj_qty[0]?$get_adj_qty[0]:0;

		$total = $s_qty + $a_qty;

		return $total;
	}

	function stock_in_qty($product_id, $date, $conn){
		$stock_in_qty = mysqli_fetch_array(mysqli_query($conn, "SELECT sum(quantity) as qty FROM `tbl_stocks` WHERE date_added = '$date' AND product_id = '$product_id'"));
		$stock_adj_qty = mysqli_fetch_array(mysqli_query($conn, "SELECT sum(qty) as aqty FROM `tbl_stocks_adjustment` WHERE qty >= 1 AND date_added = '$date' AND product_id = '$product_id'"));
		$s_qty = $stock_in_qty[0]?$stock_in_qty[0]:0;
		$a_qty = $stock_adj_qty[0]?$stock_adj_qty[0]:0;

		$total = $s_qty + $a_qty;

		return $total;
	}

	function stock_out_qty($product_id, $date, $conn){
		$stock_out_qty = mysqli_fetch_array(mysqli_query($conn, "SELECT sum(b.quantity) as qty FROM `tbl_sales_order` a INNER JOIN `tbl_sales_order_detail` b WHERE a.sales_order_id = b.sales_order_id AND a.status = 1 AND b.date_added <= '$date' AND b.product_id = '$product_id'"));
		$stock_adj_qty = mysqli_fetch_array(mysqli_query($conn, "SELECT sum(qty) as aqty FROM `tbl_stocks_adjustment` WHERE qty < 1 AND date_added = '$date' AND product_id = '$product_id'"));
		$s_qty = $stock_out_qty[0]?$stock_out_qty[0]:0;
		$a_qty = $stock_adj_qty[0]?$stock_adj_qty[0]:0;

		$total = $s_qty - $a_qty;

		return $total;
	}

	function get_remaining_qty($product_id, $date, $conn){
		$sold_qty = mysqli_fetch_array(mysqli_query($conn, "SELECT sum(b.quantity) - sum(b.returned_quantity) as qty FROM `tbl_sales_order` a INNER JOIN `tbl_sales_order_detail` b WHERE a.sales_order_id = b.sales_order_id AND a.status = 1 AND b.date_added <= '$date' AND b.product_id = '$product_id'"));

		$get_stock_qty = mysqli_fetch_array(mysqli_query($conn, "SELECT sum(quantity) as qty FROM `tbl_stocks` WHERE date_added <= '$date' AND product_id = '$product_id'"));

		$stock_adj_qty = mysqli_fetch_array(mysqli_query($conn, "SELECT sum(qty) as aqty FROM `tbl_stocks_adjustment` WHERE date_added = '$date' AND product_id = '$product_id'"));

		$total_remaining_qty = $get_stock_qty[0] - $sold_qty[0] + $stock_adj_qty[0];

		return $total_remaining_qty?$total_remaining_qty:0;
	}

	function check_balance_qty($product_id, $conn){
		$get_stock_qty = mysqli_fetch_array(mysqli_query($conn, "SELECT sum(quantity) as qty, sum(sold_quantity) as s_qty FROM `tbl_stocks` WHERE product_id = '$product_id'"));
		$total_qty = $get_stock_qty[0] - $get_stock_qty[1];

		return $total_qty?$total_qty:0;
	}

	function get_pType($pType){
		if($pType == 1){
			$type = "Card";
		}else if($pType == 2){
			$type = "Cash";
		}else if($pType == 3){
			$type = "Charge";
		}else if($pType == 4){
			$type = "Cheque";
		}else if($pType == 5){
			$type = "Gcash";
		}else if($pType == 6){
			$type = "Online";
		}else if($pType == 7){
			$type = "Zeller's";
		}else if($pType == 8){
			$type = "San Sebastian";
		}else if($pType == 9){
			$type = "Goldenfield";
		}else{
			$type = "Kio";
		}

		return $type;
	}

	function get_beg_bal($date, $brID, $conn){

		$cash_sales = mysqli_query($conn, "SELECT b.quantity, b.selling_price FROM tbl_sales_order a INNER JOIN tbl_sales_order_detail b ON a.sales_order_id = b.sales_order_id WHERE a.date_added < '$date' AND a.status = 1 AND a.branch_id = '$brID' AND a.p_type = 2");
		$cash_sales_total = 0;
		while($csrow = mysqli_fetch_array($cash_sales)){
			$cash_sales_total += $csrow[0] * $csrow[1];
		}

		$cash_in = mysqli_query($conn, "SELECT sum(amount) as amt FROM tbl_cash_adjustment WHERE date_added < '$date' AND branch_id = '$brID' AND adjustment_type = 2");
		$cash_in_total = 0;
		while($cirow = mysqli_fetch_array($cash_in)){
			$cash_in_total += $cirow[0];
		}

		$cash_out = mysqli_query($conn, "SELECT sum(amount) as amt FROM tbl_cash_adjustment WHERE date_added < '$date' AND branch_id = '$brID' AND adjustment_type = 3");
		$cash_out_total = 0;
		while($corow = mysqli_fetch_array($cash_out)){
			$cash_out_total += $corow[0];
		}

		$cash_payment_disbursement = mysqli_query($conn, "SELECT sum(amount) as amt FROM tbl_cash_adjustment WHERE date_added < '$date' AND branch_id = '$brID' AND (adjustment_type = 1 OR adjustment_type = 4)");
		$cash_payment_disbursement_total = 0;
		while($cpdrow = mysqli_fetch_array($cash_payment_disbursement)){
			$cash_payment_disbursement_total += $cpdrow[0];
		}

		$total_bb = ($cash_sales_total + $cash_in_total) - ($cash_out_total + $cash_payment_disbursement_total);

		return $total_bb;

	}

?>
