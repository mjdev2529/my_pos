<?php
	include '../../core/config.php';
	session_start();
	$branch = $_SESSION["bID"];

	$data = mysqli_query($conn,"SELECT * FROM
	 `tbl_stocks` a inner join tbl_products b on a.product_id=b.product_id inner join
	  tbl_stocks_adjustment c on b.product_id = c.product_id where b.branch_id = '$branch'");
	$response["data"] = array();
	$count = 1;
	while($row = mysqli_fetch_array($data)){
		$list = array();
		$list["count"] = $count++;
		$list["stocks_adjustment_id"] = $row["stocks_adjustment_id"];
		$list["product"] = $row["brand_name"];
		$list["qty"] = $row["qty"];
		$list["date_added"] = date("m-d-Y", strtotime($row["date_added"]));
		array_push($response["data"], $list);
	}

	echo json_encode($response);

?>