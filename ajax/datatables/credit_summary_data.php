<?php
	session_start();
	include '../../core/config.php';
	$from = $_POST["fromDate"];
	$branch = $_SESSION["bID"];
	$branch_data = mysqli_fetch_array(mysqli_query($conn,"SELECT branch_name, branch_address FROM tbl_users as a INNER JOIN tbl_branch b ON a.branch_id = b.branch_id WHERE a.user_id = '$_SESSION[uid]'"));
	$b_name = isset($branch_data[0])?$branch_data[0]:"Administrator";
	$b_address = isset($branch_data[1])?$branch_data[1]:"Administrator";

	function get_total_sales($p_type, $branch, $from, $conn){
		if($p_type == 11){
			$inc_cancelled = "AND (a.status = 0 OR a.status = 2)";
		}else{
			$inc_cancelled = "AND a.status != 0 AND a.p_type = '$p_type'";
		}

		$sales = mysqli_query($conn, "SELECT b.quantity, b.selling_price FROM tbl_sales_order a INNER JOIN tbl_sales_order_detail b ON a.sales_order_id = b.sales_order_id WHERE a.date_added = '$from' AND a.branch_id = '$branch' $inc_cancelled");
		$sales_total = 0;
		while($data = mysqli_fetch_array($sales)){
			$sales_total += $data[0] * $data[1];
		}

		return $sales_total;
	}
?>

<div class="col-6">
	<div class="col-2 offset-10">
		<button type="button" class="btn btn-primary" onclick="printDiv()">Print <i class="fa fa-print"></i></button>
	</div>
</div>
<div class="col-12" id="summary_container">
	<div class="col-6">
		<div class="col-12 text-center mb-5">
			<?=strtoupper($b_name)?>
			<br>
			<?=strtoupper($b_address)?>
			<br>
			<b>CREDIT TYPE SUMMARY</b>
			<br>
			<?=$from?>
		</div>
		<hr>
		<div class="row">
			<div class="col-9">DESCRIPTION</div>
			<div class="col-3 text-right pl-0">TOTAL</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-9">CARD SALES</div>
			<div class="col-3 text-right pl-0"><?=number_format(get_total_sales(1, $branch, $from, $conn),2)?></div>
			<div class="col-9">CASH SALES</div>
			<div class="col-3 text-right pl-0"><?=number_format(get_total_sales(2, $branch, $from, $conn),2)?></div>
			<div class="col-9">CHARGE SALES</div>
			<div class="col-3 text-right pl-0"><?=number_format(get_total_sales(3, $branch, $from, $conn),2)?></div>
			<div class="col-9">CHEQUE SALES</div>
			<div class="col-3 text-right pl-0"><?=number_format(get_total_sales(4, $branch, $from, $conn),2)?></div>
			<div class="col-9">GCASH</div>
			<div class="col-3 text-right pl-0"><?=number_format(get_total_sales(5, $branch, $from, $conn),2)?></div>
			<div class="col-9">ONLINE</div>
			<div class="col-3 text-right pl-0"><?=number_format(get_total_sales(6, $branch, $from, $conn),2)?></div>
			<div class="col-9">ZELLER'S</div>
			<div class="col-3 text-right pl-0"><?=number_format(get_total_sales(7, $branch, $from, $conn),2)?></div>
			<div class="col-9">SAN SEBASTIAN</div>
			<div class="col-3 text-right pl-0"><?=number_format(get_total_sales(8, $branch, $from, $conn),2)?></div>
			<div class="col-9">GOLDENFIELD</div>
			<div class="col-3 text-right pl-0"><?=number_format(get_total_sales(9, $branch, $from, $conn),2)?></div>
			<div class="col-9">KIO</div>
			<div class="col-3 text-right pl-0"><?=number_format(get_total_sales(10, $branch, $from, $conn),2)?></div>
			<div class="col-9">CANCELLED SALES</div>
			<div class="col-3 text-right pl-0"><?=number_format(get_total_sales(11, $branch, $from, $conn),2)?></div>
		</div>
	</div>
</div>