<?php
	include '../../core/config.php';
	session_start();
	$branch = $_SESSION["bID"];

	$data = mysqli_query($conn,"SELECT * FROM tbl_customers WHERE branch_id = '$branch' ORDER BY customer_name ASC");
	$response["data"] = array();
	$count = 1;
	while($row = mysqli_fetch_array($data)){
		$list = array();
		$list["count"] = $count++;
		$list["customer_id"] = $row["customer_id"];
		$list["customer_name"] = $row["customer_name"];
		$list["customer_address"] = $row["customer_address"];
		array_push($response["data"], $list);
	}

	echo json_encode($response);

?>
