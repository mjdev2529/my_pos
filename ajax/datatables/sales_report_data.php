<?php
	session_start();
	include '../../core/config.php';
	$branch = $_SESSION["bID"];
	$from = $_POST["fromDate"];
	$to = $_POST["toDate"];
	$inc_cancelled = $_POST["inc_cancelled"]==1?"":"AND status = 1";
	$branch_data = mysqli_fetch_array(mysqli_query($conn,"SELECT branch_name, branch_address FROM tbl_users as a INNER JOIN tbl_branch b ON a.branch_id = b.branch_id WHERE a.user_id = '$_SESSION[uid]'"));
	$b_name = isset($branch_data[0])?$branch_data[0]:"Administrator";
	$b_address = isset($branch_data[1])?$branch_data[1]:"Administrator";

	$sales_header = mysqli_query($conn, "SELECT * FROM tbl_sales_order WHERE date_added BETWEEN '$from' AND '$to' AND branch_id = '$branch' $inc_cancelled");
	$count = mysqli_num_rows($sales_header);

	if($count != 0){
?>

<div class="col-1 offset-11">
	<button type="button" class="btn btn-primary" onclick="printDiv()">Print <i class="fa fa-print"></i></button>
</div>
<div id="sr_container" class="col-12">
	<div class="row">
		<div class="col-12 mb-5">
			<?=strtoupper($b_name)?>
			<br>
			<?=strtoupper($b_address)?>
			<br>
			<b>SALES REPORT</b>
			<br>
			FROM <?=$from?> TO <?=$to?>
		</div>
		<?php
			$grand_total = 0;
			// $sales_header = mysqli_query($conn, "SELECT * FROM tbl_sales_order WHERE date_added BETWEEN '$from' AND '$to' AND status = 1 AND branch_id = '$branch' $inc_cancelled");
			while($row = mysqli_fetch_array($sales_header)){
		?>
		<div class="col-12 mb-3 pt-3" style="border-top: 1px solid;">
			<div class="row">
				<div class="col-3">
					<div class="row">
						<div class="col-6">Name</div>
						<div class="col-6">:&nbsp;&nbsp;&nbsp;&nbsp;<?=get_customer_name($row['customer_id'],$conn)?></div>
						<div class="col-6">Remarks</div>
						<div class="col-6">:&nbsp;&nbsp;&nbsp;&nbsp;Sales</div>
						<div class="col-6">Type</div>
						<div class="col-6">:&nbsp;&nbsp;&nbsp;&nbsp;<?=get_pType($row['p_type'])?></div>
					</div>
				</div>
				<div class="col-3 offset-6">
					<div class="row">
						<div class="col-6">SO No.</div>
						<div class="col-6">:&nbsp;&nbsp;&nbsp;&nbsp;<?=$row['receipt_no']?$row['receipt_no']:"<span class='text-danger'>Cancelled</span>"?></div>
						<div class="col-6">Date</div>
						<div class="col-6">:&nbsp;&nbsp;&nbsp;&nbsp;<?=date("m/d/Y", strtotime($row['date_added']))?></div>
						<div class="col-6">Time</div>
						<div class="col-6">:&nbsp;&nbsp;&nbsp;&nbsp;<?=$row['time_added']?></div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-12 pt-2 pb-2">
			<div class="row">
				<div class="col-1">Qty</div>
				<div class="col-8">Item</div>
				<div class="col-1">Price</div>
				<div class="col-1 pl-5">Total</div>
			</div>
		</div>
		<?php
			$total = 0;

			$grand_total += get_detail_amount($row['sales_order_id'],$conn);
			$sales_details = mysqli_query($conn, "SELECT * FROM tbl_sales_order_detail WHERE sales_order_id = '$row[sales_order_id]'");
			while($row1 = mysqli_fetch_array($sales_details)){
				$price = $row1['selling_price'];//get_product_price($row1['product_id'],$conn);
				$total += $row1['quantity']*$price;;
		?>
		<div class="col-12 pt-3 mb-3">
			<div class="row">
				<div class="col-1"><?=$row1['quantity']?></div>
				<div class="col-8"><?=get_product_name($row1['product_id'],$conn);?></div>
				<div class="col-1"><?=$price?></div>
				<div class="col-1 text-right"><?=number_format($row1['quantity']*$price,2);?></div>
			</div>
		</div>
		<?php } ?>
		<div class="col-12" style="border-bottom: 1px solid;">
			<div class="row">
				<div class="col-10 font-weight-bold text-left">Total</div>
				<div class="col-1 font-weight-bold text-right"><?=number_format($total,2)?></div>	
			</div>
		</div>
		<hr>
		<?php } ?>
		<div class="col-12 mt-3 mb-3">
			<div class="row">
				<div class="col-10 font-weight-bold text-left">Grand Total</div>
				<div class="col-1 font-weight-bold text-right"><?=number_format($grand_total,2)?></div>	
			</div>
		</div>
		<?php }else{ ?>
			<div class="col-12 text-center h3">
				<i class="fas fa-info-circle"></i> No data available.
			</div>
		<?php } ?>
	</div>
</div>