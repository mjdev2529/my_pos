<?php
	include '../../core/config.php';

	$data = mysqli_query($conn,"SELECT * FROM tbl_branch");
	$response["data"] = array();
	$count = 1;
	while($row = mysqli_fetch_array($data)){
		$list = array();
		$list["count"] = $count++;
		$list["branch_id"] = $row["branch_id"];
		$list["branch_name"] = $row["branch_name"];
		$list["branch_address"] = $row["branch_address"];
		$list["branch_contact"] = $row["branch_contact"];
		array_push($response["data"], $list);
	}

	echo json_encode($response);

?>