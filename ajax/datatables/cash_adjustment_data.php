<?php
	include '../../core/config.php';
	session_start();
	$branch = $_SESSION["bID"];

	$data = mysqli_query($conn,"SELECT * FROM tbl_cash_adjustment WHERE branch_id = '$branch'");
	$response["data"] = array();
	$count = 1;
	while($row = mysqli_fetch_array($data)){
		if($row["adjustment_type"] == 1){
			$a_type = "CASH DISBURSEMENT";
		}else if($row["adjustment_type"] == 2){
			$a_type = "CASH IN";
		}else if($row["adjustment_type"] == 3){
			$a_type = "CASH OUT";
		}else{
			$a_type = "CASH PAYMENT";
		}

		$user = mysqli_fetch_array(mysqli_query($conn, "SELECT name FROM tbl_users WHERE user_id = '$row[added_by]'"));

		$list = array();
		$list["count"] = $count++;
		$list["cash_adjustment_id"] = $row["cash_adjustment_id"];
		$list["type"] = $a_type;
		$list["remarks"] = $row["remarks"];
		$list["amount"] = $row["amount"];
		$list["added_by"] = $user[0];
		$list["date_added"] = date("m-d-Y", strtotime($row["date_added"]));
		array_push($response["data"], $list);
	}

	echo json_encode($response);

?>