<?php
	session_start();
	include '../../core/config.php';
	$from = $_POST["fromDate"];
	$branch = $_SESSION["bID"];
	$branch_data = mysqli_fetch_array(mysqli_query($conn,"SELECT branch_name, branch_address FROM tbl_users as a INNER JOIN tbl_branch b ON a.branch_id = b.branch_id WHERE a.user_id = '$_SESSION[uid]'"));
	$b_name = isset($branch_data[0])?$branch_data[0]:"Administrator";
	$b_address = isset($branch_data[1])?$branch_data[1]:"Administrator";

	$beg_bal_total = get_beg_bal($from, $branch, $conn);

	$cash_sales = mysqli_query($conn, "SELECT b.quantity, b.selling_price FROM tbl_sales_order a INNER JOIN tbl_sales_order_detail b ON a.sales_order_id = b.sales_order_id WHERE a.date_added = '$from' AND a.p_type = 2 AND a.status = 1 AND a.branch_id = '$branch'");
	$cash_sales_total = 0;
	while($csrow = mysqli_fetch_array($cash_sales)){
		$cash_sales_total += $csrow[0] * $csrow[1];
	}

	$cash_in = mysqli_query($conn, "SELECT sum(amount) as amt FROM tbl_cash_adjustment WHERE date_added = '$from' AND branch_id = '$branch' AND adjustment_type = 2");
	$cash_in_total = 0;
	while($cirow = mysqli_fetch_array($cash_in)){
		$cash_in_total += $cirow[0];
	}

	$cash_out = mysqli_query($conn, "SELECT sum(amount) as amt FROM tbl_cash_adjustment WHERE date_added = '$from' AND branch_id = '$branch' AND adjustment_type = 3");
	$cash_out_total = 0;
	while($corow = mysqli_fetch_array($cash_out)){
		$cash_out_total += $corow[0];
	}

	$cash_payment_disbursement = mysqli_query($conn, "SELECT sum(amount) as amt FROM tbl_cash_adjustment WHERE date_added = '$from' AND branch_id = '$branch' AND (adjustment_type = 1 OR adjustment_type = 4)");
	$cash_payment_disbursement_total = 0;
	while($cpdrow = mysqli_fetch_array($cash_payment_disbursement)){
		$cash_payment_disbursement_total += $cpdrow[0];
	}

	$total_cash_onhand = ($beg_bal_total + $cash_sales_total + $cash_in_total) - ($cash_out_total - $cash_payment_disbursement_total);
?>

<div class="col-6">
	<div class="col-2 offset-10">
		<button type="button" class="btn btn-primary" onclick="printDiv()">Print <i class="fa fa-print"></i></button>
	</div>
</div>
<div class="col-12" id="summary_container">
	<div class="col-6">
		<div class="col-12 text-center mb-5">
			<?=strtoupper($b_name)?>
			<br>
			<?=strtoupper($b_address)?>
			<br>
			<b>CASH SUMMARY</b>
			<br>
			<?=$from?>
		</div>
		<hr>
		<div class="row">
			<div class="col-9">DESCRIPTION</div>
			<div class="col-3 text-right pl-0">TOTAL</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-9">BEGINNING BALANCE</div>
			<div class="col-3 text-right pl-0"><?=number_format($beg_bal_total,2)?></div>
			<div class="col-9">CASH SALES</div>
			<div class="col-3 text-right pl-0"><?=number_format($cash_sales_total,2)?></div>
			<div class="col-9">CASH IN</div>
			<div class="col-3 text-right pl-0"><?=number_format($cash_in_total,2)?></div>
			<div class="col-9">CASH PAYMENTS/DISBURSEMENT</div>
			<div class="col-3 text-right pl-0"><?=number_format($cash_payment_disbursement_total,2)?></div>
			<div class="col-9">CASH OUT</div>
			<div class="col-3 text-right pl-0"><?=number_format($cash_out_total,2)?></div>
			<div class="col-9">TOTAL CASH ONHAND</div>
			<div class="col-3 text-right pl-0"><?=number_format($total_cash_onhand,2)?></div>
		</div>
	</div>
</div>
