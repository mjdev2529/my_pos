<?php
	include '../../core/config.php';
	session_start();
	$branch = $_SESSION["bID"];

	$data = mysqli_query($conn,"SELECT * FROM tbl_products WHERE branch_id = '$branch' ORDER BY brand_name ASC");
	$response["data"] = array();
	$count = 1;
	while($row = mysqli_fetch_array($data)){
		$list = array();
		$list["count"] = $count++;
		$list["product_id"] = $row["product_id"];
		$list["brand_name"] = $row["brand_name"];
		$list["generic_name"] = $row["generic_name"];
		$list["category_description"] = $row["category_description"];
		$list["price"] = $row["price"];
		$list["gross_price"] = $row["gross_price"];
		array_push($response["data"], $list);
	}

	echo json_encode($response);

?>