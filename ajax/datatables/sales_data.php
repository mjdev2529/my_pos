<?php
	include '../../core/config.php';
	$sales_id = $_POST["sales_id"];

	$data = mysqli_query($conn,"SELECT * FROM tbl_sales_order a INNER JOIN tbl_sales_order_detail b ON a.sales_order_id = b.sales_order_id INNER JOIN tbl_products c ON b.product_id = c.product_id WHERE a.sales_order_id = '$sales_id'");
	$response["data"] = array();
	$count = 1;
	$date = date("Y-m-d");
	while($row = mysqli_fetch_array($data)){

		$list = array();
		$list["count"] = $count++;
		$list["sales_id"] = $row["sales_order_id"];
		$list["sales_detail_id"] = $row["sales_order_detail_id"];
		$list["product_name"] = $row["brand_name"];
		$list["product_price"] = $row["price"]!=$row["selling_price"]?$row["selling_price"]:$row["price"];
		$list["sum"] = $row["selling_price"]*$row["quantity"];
		$list["vat"] = number_format(0 ,2);
		$list["quantity"] = $row["quantity"];
		$list["remaining"] = get_remaining_qty($row["product_id"], $date, $conn) - $row["quantity"];
		array_push($response["data"], $list);
	}

	echo json_encode($response);

?>