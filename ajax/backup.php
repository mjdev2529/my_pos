<?php
	include '../core/dumper.php';

	try {
		$export_db = Shuttle_Dumper::create(array(
		    'host' => 'localhost',
		    'username' => 'root',
		    'password' => '',
		    'db_name' => 'my_pos',
		));
		$filename = '../core/db_backup/my_pos-'.date("m-d-Y").'.sql';
		// dump the database to plain text file
		$export_db->dump($filename);
		echo "Back-up success!";
	} catch(Shuttle_Exception $e) {
		echo "Couldn't back-up database: " . $e->getMessage();
	}
?>