-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.18-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping data for table my_pos.tbl_branch: ~1 rows (approximately)
/*!40000 ALTER TABLE `tbl_branch` DISABLE KEYS */;
INSERT INTO `tbl_branch` (`branch_id`, `branch_name`, `branch_address`, `branch_contact`) VALUES
	(1, 'test branch', 'test address', '09123456789');
/*!40000 ALTER TABLE `tbl_branch` ENABLE KEYS */;

-- Dumping data for table my_pos.tbl_customers: ~1 rows (approximately)
/*!40000 ALTER TABLE `tbl_customers` DISABLE KEYS */;
INSERT INTO `tbl_customers` (`customer_id`, `customer_name`, `customer_address`, `branch_id`) VALUES
	(1, 'test customer', 'test address', 1);
/*!40000 ALTER TABLE `tbl_customers` ENABLE KEYS */;

-- Dumping data for table my_pos.tbl_products: ~4 rows (approximately)
/*!40000 ALTER TABLE `tbl_products` DISABLE KEYS */;
INSERT INTO `tbl_products` (`product_id`, `brand_name`, `generic_name`, `category_description`, `price`, `gross_price`, `is_vatable`, `branch_id`) VALUES
	(1, 'product a', 'test gen', 'test', 50.00, 57.00, 1, 1),
	(2, 'product b', 'test gen', 'test', 5.00, 5.00, 0, 0),
	(3, 'product c', 'gen', 'test', 7.00, 8.00, 1, 1),
	(5, 'product d', 'test23', 'tyest', 55.00, 57.00, 1, 1);
/*!40000 ALTER TABLE `tbl_products` ENABLE KEYS */;

-- Dumping data for table my_pos.tbl_sales_order: ~9 rows (approximately)
/*!40000 ALTER TABLE `tbl_sales_order` DISABLE KEYS */;
INSERT INTO `tbl_sales_order` (`sales_order_id`, `receipt_no`, `customer_id`, `status`, `date_updated`, `date_added`, `time_added`, `user_id`, `cash_tendered`, `is_discounted`, `p_type`, `branch_id`) VALUES
	(1, '210531091858', 1, '1', '0000-00-00 00:00:00', '2021-05-31 00:00:00', '', 1, 270.00, 0, 1, 1),
	(2, '', 0, '0', '0000-00-00 00:00:00', '2021-05-31 00:00:00', '', 0, 0.00, 0, 0, 1),
	(3, '210531092115', 1, '1', '0000-00-00 00:00:00', '2021-05-31 00:00:00', '03:04 AM', 1, 2000.00, 0, 0, 1),
	(4, '', 0, '2', '0000-00-00 00:00:00', '2021-06-19 00:00:00', '03:04 AM', 0, 0.00, 0, 0, 1),
	(5, '', 0, '0', '0000-00-00 00:00:00', '2021-06-19 00:00:00', '03:04 AM', 0, 0.00, 0, 0, 1),
	(6, '210619024016', 1, '1', '0000-00-00 00:00:00', '2021-06-19 00:00:00', '03:04 AM', 1, 62.00, 0, 2, 1),
	(7, '00000000', 1, '1', '0000-00-00 00:00:00', '2021-06-19 00:00:00', '03:04 AM', 1, 10.00, 0, 1, 1),
	(8, '00000008', 1, '1', '0000-00-00 00:00:00', '2021-06-19 00:00:00', '03:04 AM', 1, 10.00, 0, 1, 1),
	(9, '00000009', 1, '1', '0000-00-00 00:00:00', '2021-06-19 00:00:00', '03:04 AM', 1, 62.00, 0, 3, 1);
/*!40000 ALTER TABLE `tbl_sales_order` ENABLE KEYS */;

-- Dumping data for table my_pos.tbl_sales_order_detail: ~8 rows (approximately)
/*!40000 ALTER TABLE `tbl_sales_order_detail` DISABLE KEYS */;
INSERT INTO `tbl_sales_order_detail` (`sales_order_detail_id`, `sales_order_id`, `product_id`, `stock_id`, `quantity`, `returned_quantity`, `selling_price`, `gross_price`, `date_added`, `date_updated`, `status`, `discount`) VALUES
	(1, 1, 1, 16, 5, 0, 50.00, 57.00, '2021-05-31 00:00:00', '0000-00-00 00:00:00', '0', 0.00),
	(2, 2, 1, 0, 33, 0, 50.00, 57.00, '2021-05-31 00:00:00', '0000-00-00 00:00:00', '0', 0.00),
	(3, 3, 1, 16, 12, 0, 50.00, 57.00, '2021-05-31 00:00:00', '0000-00-00 00:00:00', '0', 0.00),
	(4, 3, 1, 11, 20, 0, 0.00, 0.00, '2021-05-31 00:00:00', '0000-00-00 00:00:00', '1', 0.00),
	(7, 6, 5, 18, 1, 0, 55.00, 57.00, '2021-06-19 00:00:00', '0000-00-00 00:00:00', '0', 0.00),
	(8, 7, 3, 17, 1, 0, 7.00, 8.00, '2021-06-19 00:00:00', '0000-00-00 00:00:00', '0', 0.00),
	(9, 8, 3, 17, 1, 0, 7.00, 8.00, '2021-06-19 00:00:00', '0000-00-00 00:00:00', '0', 0.00),
	(10, 9, 5, 18, 1, 0, 55.00, 57.00, '2021-06-19 00:00:00', '0000-00-00 00:00:00', '0', 0.00);
/*!40000 ALTER TABLE `tbl_sales_order_detail` ENABLE KEYS */;

-- Dumping data for table my_pos.tbl_stocks: ~7 rows (approximately)
/*!40000 ALTER TABLE `tbl_stocks` DISABLE KEYS */;
INSERT INTO `tbl_stocks` (`stock_id`, `product_id`, `supplier_id`, `cost_price`, `quantity`, `sold_quantity`, `returned_quantity`, `lot_no`, `expiry_date`, `date_added`, `date_updated`, `date_returned`, `branch_id`) VALUES
	(11, 1, 1, 5.00, 60, 20, 0, '55', '2021-06-30', '2021-05-18 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
	(12, 2, 10, 10.00, 55, 0, 0, '455', '2021-05-21', '2021-05-18 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
	(13, 3, 1, 24.00, 69, 0, 0, '124test', '2021-07-24', '2021-05-18 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
	(15, 5, 1, 59.00, 59, 0, 0, 'test', '2021-05-28', '2021-05-20 00:00:00', '2021-05-20 00:00:00', '0000-00-00 00:00:00', 1),
	(16, 1, 1, 22.00, 17, 17, 0, '123123', '2021-06-04', '2021-05-30 00:00:00', '2021-05-30 00:00:00', '0000-00-00 00:00:00', 1),
	(17, 3, 10, 22.00, 10, 2, 0, '124132', '2021-05-01', '2021-05-30 00:00:00', '2021-05-30 00:00:00', '2021-05-30 00:00:00', 1),
	(18, 5, 1, 500.00, 1000, 2, 0, '', '0000-00-00', '2021-06-19 00:00:00', '2021-06-19 00:00:00', '2021-06-19 00:00:00', 1);
/*!40000 ALTER TABLE `tbl_stocks` ENABLE KEYS */;

-- Dumping data for table my_pos.tbl_supplier: ~2 rows (approximately)
/*!40000 ALTER TABLE `tbl_supplier` DISABLE KEYS */;
INSERT INTO `tbl_supplier` (`supplier_id`, `supplier_name`, `branch_id`) VALUES
	(1, 'test supplier', 1),
	(10, 'test supplier 1', 1);
/*!40000 ALTER TABLE `tbl_supplier` ENABLE KEYS */;

-- Dumping data for table my_pos.tbl_users: ~2 rows (approximately)
/*!40000 ALTER TABLE `tbl_users` DISABLE KEYS */;
INSERT INTO `tbl_users` (`user_id`, `name`, `username`, `password`, `role`, `branch_id`) VALUES
	(1, 'Admin', 'admin', '81dc9bdb52d04dc20036dbd8313ed055', 0, 0),
	(2, 'staff 1', 'a', '0cc175b9c0f1b6a831c399e269772661', 0, 1);
/*!40000 ALTER TABLE `tbl_users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
